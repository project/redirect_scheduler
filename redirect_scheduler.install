<?php

/**
 * @file
 * Install, update and uninstall functions for the redirect scheduler module.
 */

/**
 * Implements hook_schema().
 */
function redirect_scheduler_schema() {
  $schema['redirect_schedule'] = array(
    'description' => 'Stores information on redirects schedules.',
    'fields' => array(
      'rid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {redirect}.uid of the redirect.',
      ),
      'activate_on' => array(
        'description' => 'The UNIX UTC timestamp when to activate',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'deactivate_on' => array(
        'description' => 'The UNIX UTC timestamp when to deactivate',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'one_time_access' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Whether user accesses this redirect just one time.',
      ),
    ),
    'primary key' => array('rid'),
    'indexes' => array(
      'redirect_scheduler_activate_on' => array('activate_on'),
      'redirect_scheduler_deactivate_on' => array('deactivate_on'),
      'redirect_scheduler_one_time_access' => array('one_time_access'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function redirect_scheduler_install() {
  // If the redirect table exists, then set the schema to run the
  // migration update function.
  if (db_table_exists('redirect')) {
    drupal_set_installed_schema_version('redirect_schedule', 6999);
  }
}

/**
 * Implements hook_uninstall().
 */
function redirect_scheduler_uninstall() {
  drupal_load('module', 'redirect_scheduler');
  $variables = array_keys(redirect_scheduler_variables());
  foreach ($variables as $variable) {
    variable_del($variable);
  }
}