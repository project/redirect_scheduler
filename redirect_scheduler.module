<?php

/**
 * @file Redirect scheduler module
 */

function redirect_scheduler_variables() {
  return array(
  );
}

/**
 * Implements hook_entity_info().
 */
function redirect_scheduler_entity_info() {
  $info['redirect_schedule'] = array(
    'label' => t('Redirect Schedule'),
    'base table' => 'redirect_schedule',
    'entity keys' => array(
      'id' => 'rid',
    ),
    'fieldable' => FALSE,
    'uuid' => FALSE,
    'redirect' => FALSE,
  );

  return $info;
}

/**
 * Implements hook_permission().
 */
function redirect_scheduler_permission() {
  $permissions['administer redirect_schedules'] = array(
    'title' => t('Administer URL redirections schedules'),
  );
  return $permissions;
}

/**
 * Implements hook_form_alter().
 */
function redirect_scheduler_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'redirect_edit_form') {
    $redirect_schedule = new stdClass();
    redirect_schedule_object_prepare($redirect_schedule);

    if ($form['rid']['#value']) {
      $rid = $form['rid']['#value'];
      $redirect_schedule = redirect_scheduler_load($rid);

      if (!is_object($redirect_schedule)) {
        $redirect_schedule = new stdClass();
        redirect_schedule_object_prepare($redirect_schedule);
      }
    }

    $form['schedule'] = array(
      '#type' => 'fieldset',
      '#title' => t('Schedule'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['schedule']['activate_on'] = array(
      '#type' => 'textfield',
      '#title' => t('Activate on'),
      '#date_format' => variable_get('date_format_short', 'm/d/Y H:i'),
      '#default_value' => $redirect_schedule->activate_on > 0?date('Y-m-d H:i:s', $redirect_schedule->activate_on):NULL,
    );

    $form['schedule']['deactivate_on'] = array(
      '#type' => 'textfield',
      '#title' => t('Deactivate on'),
      '#date_format' => variable_get('date_format_short', 'm/d/Y H:i'),
      '#default_value' => $redirect_schedule->deactivate_on > 0?date('Y-m-d H:i:s', $redirect_schedule->deactivate_on):NULL,
    );

    if (module_exists('date_popup')) {
      $form['schedule']['activate_on']['#type'] = 'date_popup';
      $form['schedule']['deactivate_on']['#type'] = 'date_popup';
    }

    $form['schedule']['one_time_access'] = array(
      '#type' => 'checkbox',
      '#title' => t('One time access'),
      '#description' => t('By checking this option, the redirection will be active only on the first time the user access the source URL.'),
      '#default_value' => isset($redirect_schedule)?$redirect_schedule->one_time_access:0,
    );

    $form['#submit'][] = 'redirect_scheduler_form_submit';
  }
}

/**
 * Form submit.
 */
function redirect_scheduler_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);

  // Convert date/time string to unix timestamp.
  try {
    $activate_on = DateTime::createFromFormat('Y-m-d H:i', $form_state['values']['activate_on']);
    $deactivate_on = DateTime::createFromFormat('Y-m-d H:i', $form_state['values']['deactivate_on']);

    $form_state['values']['activate_on'] = $activate_on?$activate_on->getTimestamp():0;
    $form_state['values']['deactivate_on'] = $deactivate_on?$deactivate_on->getTimestamp():0;
  }
  catch (Exception $ex) {
  }

  $redirect_schedule = (object) $form_state['values'];
  $redirect_schedule->is_new = FALSE;

  // Check if redirect is new by checking if rid is null.
  if ( is_null($form_state['values']['rid']) ) {
    //Yep, it's new. Let's retrieve the last redirect id. Is there a smarter way of doing it?
    $last_rid = db_query('SELECT MAX(rid) FROM {redirect}')->fetchField();
    $redirect_schedule->rid = $last_rid;
    $redirect_schedule->is_new = TRUE;
  } 
  else {
    // Check if redirect_schedule is new.
    $redirect_schedule_load = redirect_scheduler_load($form_state['values']['rid']);
    if (!$redirect_schedule_load) {
      $redirect_schedule->is_new = TRUE;
    }
  }

  redirect_scheduler_save($redirect_schedule);
}

/**
 * Redirect schedule object prepare.
 */
function redirect_schedule_object_prepare(&$redirect_schedule, $defaults = array()) {
  $defaults += array(
    'rid' => NULL,
    'activate_on' => 0,
    'deactivate_on' => 0,
    'one_time_access' => 0,
  );

  foreach ($defaults as $key => $default) {
    if (!isset($redirect_schedule->{$key})) {
      $redirect_schedule->{$key} = $default;
    }
  }
}

/**
 * Load an redirect schedule from the database.
 *
 * @param $rid
 *   The URL redirect ID.
 * @param $reset
 *   Whether to reset the redirect_load_multiple cache.
 *
 * @return
 *   An redirect schedule object, or FALSE if loading failed.
 *
 * @ingroup redirect_api
 */
function redirect_scheduler_load($rid, $reset = FALSE) {
  $redirect_schedules = entity_load('redirect_schedule', array($rid), array(), $reset);
  return !empty($redirect_schedules) ? reset($redirect_schedules) : FALSE;
}

/**
 * Save an URL redirect.
 *
 * @param $redirect
 *   The URL redirect object to be saved. If $redirect->rid is omitted (or
 *   $redirect->is_new is TRUE), a new redirect will be added.
 *
 * @ingroup redirect_api
 */
function redirect_scheduler_save($redirect_schedule) {
  $transaction = db_transaction();

  try {
    if (!empty($redirect_schedule->rid) && !isset($redirect_schedule->original)) {
      $redirect_schedule->original = entity_load_unchanged('redirect_schedule', $redirect_schedule->rid);
    }

    // Determine if we will be inserting a new node.
    if (!isset($redirect_schedule->is_new)) {
      $redirect_schedule->is_new = empty($redirect_schedule->rid);
    }

    // Allow other modules to alter the redirect before saving.
    module_invoke_all('entity_presave', $redirect_schedule, 'redirect_schedule');

    // Save the redirect to the database and invoke the post-save hooks.
    if ($redirect_schedule->is_new) {
      drupal_write_record('redirect_schedule', $redirect_schedule);
      module_invoke_all('entity_insert', $redirect_schedule, 'redirect_schedule');
    }
    else {
      drupal_write_record('redirect_schedule', $redirect_schedule, array('rid'));
      module_invoke_all('entity_update', $redirect_schedule, 'redirect_schedule');
    }

    // Clear internal properties.
    unset($redirect_schedule->is_new);
    unset($redirect_schedule->original);

    // Clear the static loading cache.
    entity_get_controller('redirect_schedule')->resetCache(array($redirect_schedule->rid));

    // Ignore slave server temporarily to give time for the
    // saved node to be propagated to the slave.
    db_ignore_slave();
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('redirect_schedule', $e);
    throw $e;
  }
}

/**
 * Implements hook_redirect_alter(). Where the magic happens.
 */
function redirect_scheduler_redirect_alter(&$redirect) {
  // Load redirect schedule.
  $redirect_schedule = redirect_scheduler_load($redirect->rid);

  if ($redirect_schedule) {
    $now = strtotime("now");

    if ($redirect_schedule->one_time_access == 1) {
      $cookie_key = 'redirect_access_' . $redirect->rid;

      if (isset($_COOKIE['Drupal_visitor_' . $cookie_key])) {
        $user_cookie = $_COOKIE['Drupal_visitor_' . $cookie_key];
      }

      if (isset($user_cookie)) {
        unset($redirect->callback);
      }
      else {
        user_cookie_save(array($cookie_key => '1'));
      }
    }

    if ($redirect_schedule->activate_on == 0 && $redirect_schedule->deactivate_on == 0) {
      return TRUE;
    }

    if ($redirect_schedule->activate_on == 0 && $redirect_schedule->deactivate_on >= $now) {
      return TRUE;
    }

    if ($redirect_schedule->activate_on <= $now && $redirect_schedule->deactivate_on == 0) {
      return TRUE;
    }

    if ($redirect_schedule->activate_on <= $now && $redirect_schedule->deactivate_on >= $now) {
      return TRUE;
    }

  }
  else {
    return TRUE;
  }

  unset($redirect->callback);
}
